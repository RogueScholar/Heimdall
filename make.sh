#!/usr/bin/env bash
# echo Install Homebrew
# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# Install dependencies
# brew install libusb libusb-compat qt5 cmake

if [ "x$1" = "x" ]; then
  PREFIX=/usr
else
  PREFIX="$1"
fi

mkdir -p build
rm -rf ./build/*
cd build || exit 1
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX:PATH="$PREFIX" -DCMAKE_BUILD_TYPE=Release ..
make -j"$(nproc)"

if [ ! $EUID = 0 ]; then
  exec sudo make install
fi
